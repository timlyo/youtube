import flask

app = flask.Flask(__name__)

from website import pages
from website import api
from website import filters
