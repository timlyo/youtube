from dateutil import parser
from datetime import datetime

from website import app
from website import database

from flask import jsonify, request, render_template

from . import search

@app.route("/api/videos")
def get_videos():
	channel = request.args.get("channel")
	query = request.args.get("filter")

	after = request.args.get("after")
	if after:
		after = parser.parse(after)
		assert(after.tzinfo)

	before = request.args.get("before")
	if before:
		before = parser.parse(before)

	# List of videos from that channel and within that date
	videos = database.get_videos(channel=channel, after=after, before=before)
	print("Returning {} videos".format(len(videos)))

	if query == "":
		return render_template("image_grid.html", videos=videos)

	videos = search.order_results(query, videos)
	return render_template("image_grid.html", videos=videos)
