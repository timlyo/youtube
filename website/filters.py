from website import app
from flask import Markup
from markdown2 import Markdown

from datetime import datetime

@app.template_filter("markdown")
def markdown(s):
	return Markup(Markdown().convert(s))

@app.template_filter("readable_date")
def date_filter(date):
	assert isinstance(date, datetime)
	return date.strftime(r"%Y-%b-%d")
