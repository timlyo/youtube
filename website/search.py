from difflib import SequenceMatcher
from sortedcontainers import SortedListWithKey

def order_results(query, videos):
    results = SortedListWithKey(key=lambda video: video["score"])
    # give video a score
    for video in videos:
        score = 0
        score += score_title(query, video["title"])

        score += score_tags(query, video["tags"])

        video["score"] = score
        results.add(video)
    return reversed(results)

def score_title(query: str, title: str) -> float:
    score = 0
    for query_word in query.split(" "):
        for title_word in title.split(" "):
            similarity = SequenceMatcher(None, title_word, query_word).ratio()
            if similarity > 0.65:
                score += 100
    return score

def score_tags(query: str, tags: list) -> float:
    score = 0
    for word in query.split(" "):
        for tag in tags:
            similarity = SequenceMatcher(None, word, tag).ratio()
            if similarity > 0.65:
                score += 50
    return score
