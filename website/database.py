import rethinkdb
import datetime
import pprint
from typing import List

printer = pprint.PrettyPrinter().pprint

from Levenshtein import distance

def get_videos(channel=None, before=None, after=None, amount=50) -> List:
	""" Video get function """
	rethinkdb.connect("localhost", 28015).repl()
	query = rethinkdb.db("test").table("videos")

	if after and before:
		assert(isinstance(after, datetime.datetime))
		assert(isinstance(before, datetime.datetime))
		query = query.between(after, before, index="date")
	elif after:
		query = query.filter(rethinkdb.row["date"] > after)
	elif before:
		query = query.filter(rethinkdb.row["date"] < before)

	if channel:
		assert(isinstance(channel, str))
		query = query.filter({"channel": channel})

	query = query.order_by(rethinkdb.desc("date"))
	query = query.limit(amount)

	return list(query.run())



def get_video(video_id):
	""" Return a single video's information """
	rethinkdb.connect("localhost", 28015).repl()
	query = rethinkdb.db("test").table("videos").get(video_id)
	result = query.run()
	return result

def filter_from_query(filter, query):
	scores = {}
	for video in query.run():
		score = distance(filter, video["title"])
		if score > 50:
			continue
		scores[video["title"]] = [score]

	printer(scores)

def get_channel(channel_id):
	""" Returns channel info from either an id or a name"""
	rethinkdb.connect("localhost", 28015).repl()
	table = rethinkdb.db("test").table("channels")

	channel = table.get(channel_id).run()
	if channel:
		return channel

	#TODO case insesetive match
	channel = list(table.get_all(channel_id, index="title").run())
	if len(channel) == 1:
		return channel[0]
