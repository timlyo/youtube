from flask import render_template
from website import app, database


@app.route("/")
def index():
    videos = database.get_videos(amount=50)
    print(videos[0])
    return render_template("index.html", videos=videos)

@app.route("/watch/<id>")
def watch(id):
    video = database.get_video(id)
    return render_template("watch.html", video=video)


@app.route("/channel/<channel_id>")
def channel(channel_id):
    channel = database.get_channel(channel_id)
    if channel:
        return render_template("channel.html", channel=channel)
    else:
        return render_template("channel_404.html", channel_id=channel_id)
