from website import app
import os, glob

if __name__ == "__main__":
	print("Running from", os.getcwd())
	haml_templates = glob.glob("haml/*.haml")
	javascript = glob.glob("javascript/*.js")
	app.run(debug=True, extra_files=haml_templates+javascript)
