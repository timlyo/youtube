#Patterns

all: templates javascript css

clean:
	rm -r website/static
	rm -r website/templates

templates:\
	website/templates/image_grid.html\
	website/templates/index.html\
	website/templates/layout.html\
	website/templates/navigation.html\
	website/templates/channel.html\
	website/templates/channel_404.html\
	website/templates/watch.html\

javascript:\
	website/static/js/data.js\
	website/static/js/init.js\
	website/static/js/interface.js\
	website/static/js/player.js\

css:\
	website/static/css/style.css\
	website/static/css/channel.css\

install: all
	sudo mkdir -p /srv/http/youtube-thing
	sudo cp -r website /srv/http/youtube-thing/
	sudo cp nginx.conf /srv/http/youtube-thing/
	sudo cp uwsgi.ini /srv/http/youtube-thing/
	sudo cp main.py /srv/http/youtube-thing/

	sudo ln -sf /srv/http/youtube-thing/uwsgi.ini /etc/uwsgi/vassals/youtube-thing.ini
	sudo mkdir -p /var/log/youtube-thing
	sudo chown http:log /var/log/youtube-thing
	sudo chown -R http:http /srv/http/youtube-thing

	sudo systemctl restart emperor.uwsgi

# patterns
website/templates/%.html: haml/%.haml
	@mkdir -p website/templates
	haml $< $@

website/static/js/%.js: javascript/%.js
	@mkdir -p website/static/js
	cp $< $@

website/static/css/%.css: sass/%.sass
	@mkdir -p website/static/css
	sass $< $@
