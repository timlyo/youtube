import unittest
import datetime

from website import search
import rethinkdb

rethinkdb.connect("localhost", 28015).repl()


class TestOrdering(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.videos = []
        cls.videos.append(rethinkdb.db("test").table("videos").get("-3aX7WQ1Wpc").run())
        cls.videos.append(rethinkdb.db("test").table("videos").get("-4kR3xXFzWM").run())
        cls.videos.append(rethinkdb.db("test").table("videos").get("-dnmzWiqikk").run())
        cls.videos.append(rethinkdb.db("test").table("videos").get("2S72M3-j3SE").run())

    def test_hexcells(self):
        results = list(search.order_results("Hexcells", self.videos))
        for video in self.videos:
            print(video["title"], ":", video["score"], ":", video["id"])
        assert results[0]["id"] == "-dnmzWiqikk"
        assert results[0]["score"] > 0

    def test_prop_hunt(self):
        results = list(search.order_results("prop", self.videos))
        for video in self.videos:
            print(video["title"], ":", video["score"], ":", video["id"])
        assert results[0]["id"] == "-3aX7WQ1Wpc"
        assert results[0]["score"] > 0

class TestTitleMatch(unittest.TestCase):

    def test_match(self):
        assert search.score_title("lotr", "lotr") > 0
        assert search.score_title("lotr", "lotr war in the north") > 0
        assert search.score_title(
            "lotr", "lah lah lah lotr war in the north") > 0

    def test_multi_word_match(self):
        assert search.score_title("prop hunt", "prop hunt with the_spunge") > 0

    def test_no_match(self):
        assert search.score_title("lotr", "") == 0
        assert search.score_title("lotr", "potato") == 0

class TestTagMatch(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.tags = ["TF2", "Asia", "orangutan"]

    def test_match(self):
        assert search.score_tags("TF2", self.tags) > 0

    def test_no_match(self):
        assert search.score_tags("moose", self.tags) == 0
