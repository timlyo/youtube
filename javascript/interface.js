class Interface {
	static install_search_handler() {
		$("#filter").keyup($.debounce(250, Interface.search));
		$("#after").change($.debounce(250, Interface.search));
		$("#before").change($.debounce(250, Interface.search));
		$("#channel").change($.debounce(250, Interface.search));
	}

	static search() {
		let filter = $("#filter").children("input").val();
		let before = new Date($("#before").val());
		let after = new Date($("#after").val());
		let channel = $("#channel").val();
		console.log(filter);
		Data.load_videos(channel, before, after, filter);
	}

	static update_thumbnails(videos) {
		$("#video_list").html(videos);
		// Interface.format_dates();
	}

	static show_advanced_search(){
		$("#advanced_search").show();
		$("#filter>button").click(function() {
			Interface.hide_advanced_search()
		});
		$("#filter>button").html("<i class='fa fa-angle-up'></i>");
	}

	static hide_advanced_search(animate=false){
		$("#advanced_search").hide();
		$("#filter>button").click(function() {
			Interface.show_advanced_search()
		});
		$("#filter>button").html("<i class='fa fa-angle-down'></i>");
	}

	static display_no_vidoes_found(){
		// TODO
	}

	static format_dates(){
		console.log("Formatting dates");
		jQuery(document).ready(function() {
			jQuery("time.timeago").timeago();
		});
	}
}
