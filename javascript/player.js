let tag = document.createElement('script');
tag.src = "https://www.youtube.com/player_api";
let firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

let player;
let video_id = $("#video_player").attr("video_id");
console.log("id:", video_id);

let width = $("#video_player").width();

function onYouTubePlayerAPIReady() {
	player = new YT.Player('video_player', {
		height: (width/16)*9,
		width: width,
		videoId: video_id,
		playerVars: {
			autoplay: true
		}
	});

	// console.log(player);
	// player.playVideo();
}
