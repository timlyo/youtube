class Data {
	static load_videos(channel, before, after, filter) {
		let url = "/api/videos?";
		let parameters = [];
		if (channel !== undefined) {
			parameters.push("channel=" + channel);
		}

		if (before.toString() !== "Invalid Date") {
			parameters.push("before=" + before.toISOString());
		}

		if (after.toString() !== "Invalid Date") {
			parameters.push("after=" + after.toISOString());
		}

		if (filter !== undefined) {
			parameters.push("filter=" + filter);
		}

		$.get({
			url: url + parameters.join("&"),
			success: (data) => {
				Interface.update_thumbnails(data);
			}
		})
	}
}
